/*
 * xclipglue: unify two X servers' clipboards.
 */

#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using std::clog;
using std::cout;
using std::endl;
using std::function;
using std::make_unique;
using std::map;
using std::min;
using std::move;
using std::ostream;
using std::ostringstream;
using std::pair;
using std::queue;
using std::set;
using std::string;
using std::swap;
using std::unique_ptr;
using std::vector;

#include <unistd.h>
#include <poll.h>
#include <xcb/xcb.h>

class ErrorExit {
    bool print_errno = false;
    bool notdone = true;
    int exitstatus;
  public:
    ostream &os() { return clog; }
    ErrorExit(bool err = false, int status = 1) :
        print_errno(err), exitstatus(status) {
        os() << "xclipglue: ";
    }
    bool not_done() { bool toret = notdone; notdone = false; return toret; }
    ~ErrorExit() {
        if (print_errno)
            os() << ": " << strerror(errno);
        os() << endl;
        exit(exitstatus);
    }
};
#define FATAL(...) for (ErrorExit ee(__VA_ARGS__); ee.not_done() ;) ee.os()

static const char base64digits[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char hexdigits[] = "0123456789ABCDEF";

static constexpr inline uint32_t cval(unsigned char c) { return c; }

class Codec {
  protected:
    const string &s;
    virtual void write_to(ostream &os) const = 0;
  public:
    Codec(const string &s) : s(s) {}
    virtual ~Codec() = default;
    operator string() const {
        ostringstream os;
        write_to(os);
        return os.str();
    }
    friend ostream &operator<<(ostream &os, const Codec &codec) {
        codec.write_to(os);
        return os;
    }
};

class Base64Encoder : public Codec {
    void write_to(ostream &os) const override {
        for (size_t pos = 0, end = s.size(); pos < end; pos += 3) {
            size_t len = min<size_t>(end-pos, 3);
            uint32_t val = 0;
            for (size_t i = 0; i < len; i++)
                val |= cval(s[pos+i]) << (16 - 8*i);
            for (size_t i = 0; i < len+1; i++)
                os << base64digits[0x3F & (val >> (18 - 6*i))];
            for (size_t i = len+1; i < 4; i++)
                os << '=';
        }
    }
  public:
    using Codec::Codec;
};

class Base64Decoder : public Codec {
    unsigned char lookup[256];
    void write_to(ostream &os) const override {
        size_t end = s.find_last_not_of("=");
        end = (end == string::npos) ? 0 : end + 1;
        for (size_t pos = 0; pos < end; pos += 4) {
            size_t len = min<size_t>(end-pos, 4);
            uint32_t val = 0;
            for (size_t i = 0; i < len; i++)
                val |= lookup[cval(s[pos+i])] << (18 - 6*i);
            for (size_t i = 0; i+1 < len; i++)
                os << static_cast<char>(val >> (16 - 8*i));
        }
    }
  public:
    Base64Decoder(const string &s) : Codec(s) {
        memset(lookup, 0, 256);
        for (size_t i = 0; i < 64; i++)
            lookup[cval(base64digits[i])] = i;
    }
};

class PercentEscaper : public Codec {
    uint32_t to_escape[256/32] = {
        // Default: let through printable ASCII, except for %
        0xFFFFFFFF, 0x00000020, 0x00000000, 0x80000000,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF
    };
    bool encode_space_as_underscore;
    inline bool get(unsigned char c) const {
        return 1 & (to_escape[c/32] >> (c%32));
    }
    inline void set(unsigned char c) {
        to_escape[c/32] |= 1 << (c%32);
    }
    void write_to(ostream &os) const override {
        for (char c: s) {
            if (c == ' ' && encode_space_as_underscore)
                os << '_';
            else if (get(c))
                os << '%' << hexdigits[cval(c) >> 4]
                   << hexdigits[cval(c) & 0xF];
            else
                os << c;
        }
    }
  public:
    PercentEscaper(const string &s, const string &avoid,
                   bool underscore = false)
        : Codec(s), encode_space_as_underscore(underscore) {
        for (char c: avoid)
            set(cval(c));
        if (underscore)
            set(cval('_'));
    }
};

class PercentUnescaper : public Codec {
    unsigned char lookup[256];
    bool decode_underscore_as_space;
    void write_to(ostream &os) const override {
        for (size_t pos = 0, end = s.size(); pos < end ;) {
            char c = s[pos++];
            if (c == '_' && decode_underscore_as_space) {
                c = ' ';
            } else if (c == '%') {
                unsigned val = 0;
                for (size_t i = 0; i < 2; i++)
                    if (pos < end)
                        val |= lookup[cval(s[pos++])] << (4 - 4*i);
                c = val;
            }
            os << c;
        }
    }
  public:
    PercentUnescaper(const string &s, bool underscore = false)
        : Codec(s), decode_underscore_as_space(underscore) {
        memset(lookup, 0, 256);
        for (size_t i = 0; i < 10; i++)
            lookup['0' + i] = i;
        for (size_t i = 0; i < 6; i++)
            lookup['A' + i] = lookup['a' + i] = i + 10;
    }
};

inline string our_dest_prop() { return "xclipglue-dest"; }
inline string our_multidest_prop(size_t i) {
    ostringstream oss;
    oss << "xclipglue-multidest." << i;
    return oss.str();
}

class XClipGlue {
    using event_ptr = unique_ptr<xcb_generic_event_t, decltype(&free)>;
    using event_matcher = function<bool(const xcb_generic_event_t *)>;

    static constexpr size_t INCR_THRESHOLD = 16384, INCR_BLOCKSIZE = 16384;

    struct Selection {
        xcb_atom_t atom;
        xcb_timestamp_t time_acquired;
    };

    struct Prop {
        string type;
        int format;
        string data;
        Prop() = default;
        Prop(const Prop &) = default;
        Prop(const string &type, int format, const string &data)
            : type(type), format(format), data(data) {}
        Prop(const Prop &larger, size_t pos, size_t size)
            : type(larger.type), format(larger.format),
              data(larger.data.substr(pos, size)) {}
    };

    struct PendingReqW {
        xcb_atom_t selection, target, master_dest;
        xcb_timestamp_t timestamp, sel_timestamp;
        xcb_window_t window;
        bool multiple;
        vector<string> targets;
        vector<xcb_atom_t> destinations;
    };

    struct PendingReqR {
        string id;
        bool multiple;
        size_t ntargets;
        vector<xcb_atom_t> propatoms;
        vector<unique_ptr<Prop>> results;
        bool got_sel_notify = false;
        set<size_t> incrs_in_progress;
    };

    struct IncrW {
        size_t pos;
        Prop value;
    };

    int read_fd, write_fd;
    string read_name, write_name;
    string readq, writeq;
    queue<event_ptr> eventq;

    xcb_connection_t *conn;
    xcb_window_t ownerwin;
    map<string, xcb_atom_t> atoms_by_name;
    map<xcb_atom_t, string> atoms_by_id;
    map<string, Selection> selections_owned;
    map<xcb_window_t, unique_ptr<PendingReqR>> pending_reqs_r;
    map<string, PendingReqW> pending_reqs_w;
    map<xcb_window_t, map<xcb_atom_t, IncrW>> incrs_w;
    string next_id = "0";

    void handle_command(const string &line);
    void handle_x_event(const xcb_generic_event_t *ev);
    void handle_selection_notify(const xcb_selection_notify_event_t *ev);
    void handle_selection_request(const xcb_selection_request_event_t *ev);
    void handle_selection_clear(const xcb_selection_clear_event_t *ev);
    void handle_property_notify(const xcb_property_notify_event_t *ev);

    void get_sel_property(PendingReqR &req, xcb_window_t window,
                          xcb_atom_t property);
    void read_incr(xcb_window_t window, xcb_atom_t property, size_t i);
    void send_rsp(xcb_window_t window);

    void send_command(const vector<string> &words);

    bool acquire_selection(const string &name);
    void request_selection(const string &name, const string &id,
                           const vector<string> &targets);
    xcb_window_t make_window(xcb_window_t parent);
    xcb_timestamp_t get_server_time();
    xcb_atom_t atom_from_string(string name);
    vector<xcb_atom_t> atoms_from_strings(vector<string> names);
    string atom_to_string(xcb_atom_t atom);
    vector<string> atoms_to_strings(vector<xcb_atom_t> atoms);
    void fetch_events();
    event_ptr wait_for_event(event_matcher);
    void discard_events(event_matcher discard);
    Prop get_property(xcb_window_t window, xcb_atom_t property,
                      bool delete_it = false);
    void write_property(xcb_window_t window, xcb_atom_t dest, const Prop &p);
    void write_sel_property(xcb_window_t window, xcb_atom_t dest,
                            const Prop &p);
    void write_sel_property_increment(xcb_window_t window, xcb_atom_t dest,
                                      IncrW &iw);

    string encode_prop(const Prop &p);
    bool decode_prop(const string &s, Prop &p);
    string invent_id();

    ostream *logstream = nullptr;

  public:
    void set_log_stream(ostream &os) { logstream = &os; }
    void setup_stdio();
    void setup_subcommand(const vector<string> &cmd);
    const char *setup_double_display(const char *parent_display,
                                     const char *other_display, bool *child);
    void setup_x11(const char *display);
    void send_acq(const string &sel) {
        send_command({"acq", PercentEscaper(sel, " ")});
    }
    void mainloop();
};

void XClipGlue::setup_stdio()
{
    read_fd = 0;
    write_fd = 1;
    read_name = "standard input";
    write_name = "standard output";
}

void XClipGlue::setup_subcommand(const vector<string> &subcommand)
{
    vector<const char *> args;
    for (auto &arg: subcommand)
        args.push_back(arg.c_str());
    args.push_back(nullptr);

    int rpipe[2], wpipe[2];
    if (pipe(rpipe) < 0 || pipe(wpipe) < 0)
        FATAL(true) << "pipe";

    pid_t pid = fork();

    if (pid < 0)
        FATAL(true) << "fork";

    if (pid == 0) {
        dup2(rpipe[1], 1);
        dup2(wpipe[0], 0);
        close(rpipe[0]);
        close(rpipe[1]);
        close(wpipe[0]);
        close(wpipe[1]);
        execvp(args[0], const_cast<char *const *>(args.data()));
        FATAL(true, 127) << args[0] << ": execvp";
    }

    close(rpipe[1]);
    close(wpipe[0]);
    read_fd = rpipe[0];
    write_fd = wpipe[1];
    read_name = write_name = "subcommand pipe";
}

const char *XClipGlue::setup_double_display(
    const char *parent_display, const char *child_display, bool *child)
{
    int rpipe[2], wpipe[2];
    if (pipe(rpipe) < 0 || pipe(wpipe) < 0)
        FATAL(true) << "pipe";

    pid_t pid = fork();

    if (pid < 0)
        FATAL(true) << "fork";

    read_name = write_name = "interprocess pipe";

    if (pid == 0) {
        close(rpipe[0]);
        close(wpipe[1]);
        read_fd = wpipe[0];
        write_fd = rpipe[1];
        *child = true;

        // In this fork mode, we don't want _both_ instances of
        // ourself to log the same protocol traffic. Suppress logging
        // in the child.
        logstream = nullptr;

        return child_display;
    } else {
        close(rpipe[1]);
        close(wpipe[0]);
        read_fd = rpipe[0];
        write_fd = wpipe[1];
        *child = false;
        return parent_display;
    }
}

void XClipGlue::setup_x11(const char *display)
{
    int screen_no;
    conn = xcb_connect(display, &screen_no);
    if (xcb_connection_has_error(conn))
        FATAL(false) << "error connecting to X display";

    const xcb_setup_t *setup = xcb_get_setup(conn);
    xcb_screen_iterator_t screen_iter = xcb_setup_roots_iterator(setup);
    for (int i = 0; i < screen_no; i++)
        xcb_screen_next(&screen_iter);
    xcb_screen_t *screen = screen_iter.data;

    ownerwin = make_window(screen->root);
}

void XClipGlue::mainloop()
{
    pollfd fds[3];
    fds[0] = { read_fd, POLLIN };
    fds[1] = { write_fd, POLLOUT };
    fds[2] = { xcb_get_file_descriptor(conn), POLLIN };

    while (true) {
        xcb_flush(conn);

        fds[1].events = (writeq.empty() ? 0 : POLLOUT);

        fetch_events();
        if (poll(fds, 3, eventq.empty() ? -1 : 0) < 0)
            FATAL(true) << "poll";

        if (fds[0].revents & (POLLIN | POLLHUP | POLLRDHUP)) {
            char rdbuf[4096];
            int nread = read(read_fd, rdbuf, sizeof(rdbuf));
            if (nread < 0)
                FATAL(true) << read_name << ": read";
            if (nread == 0) {
                // We've seen EOF on the input side of our protocol
                // connection. This means we should exit - but first,
                // we'll finish serving any requests that we received
                // just before EOF.

                // So, for the moment, just stop trying to read from
                // read_fd.
                fds[0].events = 0;
            }

            size_t linestart = 0, pos = readq.size(), nl;
            readq += string(rdbuf, nread);
            while ((nl = readq.find('\n', pos)) != string::npos) {
                handle_command(readq.substr(linestart, nl-linestart));
                pos = linestart = nl + 1;
            }
            if (linestart > 0)
                readq = readq.substr(linestart);
        }

        if (fds[1].revents & POLLOUT) {
            int write_max = static_cast<int>(
                min<size_t>(INT_MAX, writeq.size()));
            int nwritten = write(write_fd, writeq.data(), write_max);
            if (nwritten < 0)
                FATAL(true) << write_name << ": write";
            writeq = writeq.substr(nwritten);
        }

        fetch_events();

        while (!eventq.empty()) {
            event_ptr ev = move(eventq.front());
            eventq.pop();
            handle_x_event(ev.get());
        }

        // If read_fd is closed and we have no remaining pending
        // requests, terminate.
        if (fds[0].events == 0 && pending_reqs_r.size() == 0 && writeq.empty())
            exit(0);
    }
}

static vector<string> split_string(const string &line, char sep,
                                   size_t nseps = ~(size_t)0)
{
    size_t pos = 0;
    vector<string> words;
    while (true) {
        size_t seppos = (nseps == 0 ? string::npos :
                         (nseps--, line.find(sep, pos)));
        if (seppos == string::npos) {
            words.push_back(line.substr(pos));
            return words;
        }
        words.push_back(line.substr(pos, seppos - pos));
        pos = seppos + 1;
    }
    return words;
}

string XClipGlue::encode_prop(const Prop &p)
{
    ostringstream os;

    os << PercentEscaper(p.type, ": ");
    if ((p.type == "ATOM" || p.type == "ATOM_PAIR") && p.format == 32) {
        os << ":32a";

        vector<xcb_atom_t> atoms;
        for (size_t pos = 0, end = p.data.size(); pos+4 <= end; pos += 4) {
            xcb_atom_t atom;
            memcpy(&atom, p.data.data() + pos, 4);
            if (atom != XCB_NONE)
                atoms.push_back(atom);
        }
        vector<string> names = atoms_to_strings(atoms);

        for (size_t pos = 0, j = 0, end = p.data.size();
             pos+4 <= end; pos += 4) {
            os << ":";

            xcb_atom_t atom;
            memcpy(&atom, p.data.data() + pos, 4);
            if (atom == XCB_NONE)
                os << "-";
            else
                os << PercentEscaper(names[j++], ": -");
        }
        return os.str();
    }

    switch (p.format) {
      case 8: {
        string bs = Base64Encoder(p.data);
        string ps = PercentEscaper(p.data, ": ", true);
        if (ps.size() <= bs.size())
            os << ":8p:" << ps;
        else
            os << ":8b:" << bs;
        break;
      }
      case 16: {
        union {
            char bytes[2];
            uint16_t value;
        } u;
        os << ":16i";
        for (size_t i = 0, e = p.data.size(); i+1 < e; i += 2) {
            memcpy(u.bytes, p.data.data() + i, 2);
            os << ":" << u.value;
        }
        break;
      }
      case 32: {
        union {
            char bytes[4];
            uint32_t value;
        } u;
        os << ":32i";
        for (size_t i = 0, e = p.data.size(); i+3 < e; i += 4) {
            memcpy(u.bytes, p.data.data() + i, 4);
            os << ":" << u.value;
        }
        break;
      }
    }
    return os.str();
}

bool XClipGlue::decode_prop(const string &s, Prop &p)
{
    vector<string> words = split_string(s, ':');

    if (words.size() < 2)
        return false;

    p.type = PercentUnescaper(words[0]);
    const string &encoding = words[1];

    if (encoding == "32a") {
        vector<string> names;
        for (size_t i = 2, e = words.size(); i < e; i++) {
            auto &s = words[i];
            if (s != "-")
                names.push_back(PercentUnescaper(s));
        }
        vector<xcb_atom_t> atoms = atoms_from_strings(names);
        ostringstream os;
        for (size_t i = 2, j = 0, e = words.size(); i < e; i++) {
            auto &s = words[i];
            xcb_atom_t atom = (s == "-") ? XCB_NONE : atoms[j++];
            char data[4];
            memcpy(data, &atom, 4);
            os << string(data, 4);
        }
        p.format = 32;
        p.data = os.str();
        return true;
    } else if (encoding == "8b") {
        if (words.size() != 3)
            return false;
        p.format = 8;
        p.data = Base64Decoder(words[2]);
        return true;
    } else if (encoding == "8p") {
        if (words.size() != 3)
            return false;
        p.format = 8;
        p.data = PercentUnescaper(words[2], true);
        return true;
    } else if (encoding == "16i") {
        p.format = 16;
        ostringstream os;
        for (size_t i = 2, e = words.size(); i < e; i++) {
            uint16_t val = stoul(words[i]);
            os << string(reinterpret_cast<const char *>(&val), sizeof(val));
        }
        p.data = os.str();
        return true;
    } else if (encoding == "32i") {
        p.format = 32;
        ostringstream os;
        for (size_t i = 2, e = words.size(); i < e; i++) {
            uint32_t val = stoul(words[i]);
            os << string(reinterpret_cast<const char *>(&val), sizeof(val));
        }
        p.data = os.str();
        return true;
    } else {
        return false;
    }
}

void XClipGlue::handle_command(const string &line)
{
    vector<string> words = split_string(line, ' ');
    size_t n = words.size();

    if (logstream)
        *logstream << "<<< " << line << endl;

    if (n < 1)
        FATAL(false) << "empty protocol command";
    if (words[0] == "acq") {
        // "acq" selection
        if (n != 2)
            FATAL(false) << "badly formatted acq";
        string selection = PercentUnescaper(words[1]);

        if (!acquire_selection(selection)) {
            // If we failed to acquire the selection because somebody
            // else pipped us at the post, bounce the request straight
            // back. I _hope_ this is the right thing to do: with any
            // luck, sooner or later one side or the other will
            // succeed.
            send_acq(selection);
        }
    } else if (words[0] == "req") {
        // "req" selection id atom+
        if (n < 4)
            FATAL(false) << "badly formatted req";
        string selection = PercentUnescaper(words[1]);
        string id = PercentUnescaper(words[2]);
        vector<string> targets;
        for (size_t i = 3; i < n; i++)
            targets.push_back(PercentUnescaper(words[i]));

        request_selection(selection, id, targets);
    } else if (words[0] == "rsp") {
        // "rsp" id prop*
        if (n < 3)
            FATAL(false) << "badly formatted rsp";
        string id = PercentUnescaper(words[1]);
        auto it = pending_reqs_w.find(id);
        if (it == pending_reqs_w.end())
            return;
        auto &req = it->second;
        Prop p;
        xcb_atom_t retprop = req.master_dest;
        Prop multiprop;
        bool need_multiprop_rewrite = false;
        for (size_t i = 2, e = words.size(); i < e; i++) {
            size_t j = i-2;
            if (j < req.destinations.size()) {
                if (decode_prop(words[i], p)) {
                    if (req.targets[j] == "TIMESTAMP" &&
                        req.sel_timestamp != XCB_TIME_CURRENT_TIME) {
                        // If the target requested was TIMESTAMP, then
                        // the timestamp from the other X server will
                        // be meaningless. Overwrite it with _our_
                        // timestamp from when we acquired the
                        // selection in the destination X server.
                        size_t len = min<size_t>(4, p.data.size());
                        auto tp = reinterpret_cast<char *>(&req.sel_timestamp);
                        for (size_t k = 0; k < len; k++)
                            p.data[k] = tp[k];
                    }

                    write_sel_property(req.window, req.destinations[j], p);
                } else {
                    if (req.multiple) {
                        if (!need_multiprop_rewrite) {
                            multiprop = get_property(req.window,
                                                     req.master_dest);
                            need_multiprop_rewrite = true;
                        }
                        static const xcb_atom_t replacements[2] = {
                            XCB_NONE, XCB_NONE };
                        size_t offset = 8*j;
                        if (multiprop.data.size() > offset) {
                            size_t len = min<size_t>(
                                8, multiprop.data.size() - offset);
                            auto p = reinterpret_cast<const char *>(
                                &replacements);
                            for (size_t k = 0; k < len; k++)
                                multiprop.data[offset + k] = p[k];
                        }
                    } else {
                        retprop = XCB_NONE;
                    }
                }
            }
        }
        if (need_multiprop_rewrite)
            write_property(req.window, req.master_dest, multiprop);
        xcb_selection_notify_event_t event;
        event.response_type = XCB_SELECTION_NOTIFY;
        event.time = req.timestamp;
        event.requestor = req.window;
        event.selection = req.selection;
        event.target = req.target;
        event.property = retprop;
        xcb_send_event(conn, false, req.window, 0,
                       reinterpret_cast<const char *>(&event));
        pending_reqs_w.erase(it);
    } else {
        FATAL(false) << "bad protocol command '" << words[0] << "'";
    }
}

void XClipGlue::send_command(const vector<string> &words)
{
    ostringstream os;
    const char *sep = "";
    for (auto &word: words) {
        os << sep << word;
        sep = " ";
    }
    string line = os.str();

    if (logstream)
        *logstream << ">>> " << line << endl;

    writeq += line + "\n";
}

auto XClipGlue::wait_for_event(event_matcher criterion) -> event_ptr
{
    xcb_flush(conn);
    while (true) {
        xcb_generic_event_t *ev = xcb_wait_for_event(conn);
        if (!ev)
            exit(0);
        event_ptr evp(ev, free);
        if (criterion(evp.get()))
            return evp;
        // Otherwise, push it on our queue to look at later
        eventq.push(move(evp));
    }
}

void XClipGlue::fetch_events()
{
    while (xcb_generic_event_t *ev = xcb_poll_for_event(conn))
        eventq.push(event_ptr(ev, free));

    if (xcb_connection_has_error(conn)) {
        clog << "xclipglue: X11 connection closed" << endl;
        exit(0);
    }
}

void XClipGlue::discard_events(event_matcher discard)
{
    fetch_events();
    queue<event_ptr> eventq_new;
    while (!eventq.empty()) {
        event_ptr ev = move(eventq.front());
        eventq.pop();
        if (!discard(ev.get()))
            eventq_new.push(move(ev));
    }
    swap(eventq, eventq_new);
}

const xcb_property_notify_event_t *propevent(const xcb_generic_event_t *ev)
{
    if ((ev->response_type & 0x7F) != XCB_PROPERTY_NOTIFY)
        return nullptr;
    return reinterpret_cast<const xcb_property_notify_event_t *>(ev);
}

const xcb_selection_clear_event_t *selclearevent(const xcb_generic_event_t *ev)
{
    if ((ev->response_type & 0x7F) != XCB_SELECTION_CLEAR)
        return nullptr;
    return reinterpret_cast<const xcb_selection_clear_event_t *>(ev);
}

const xcb_selection_notify_event_t *selnotevent(const xcb_generic_event_t *ev)
{
    if ((ev->response_type & 0x7F) != XCB_SELECTION_NOTIFY)
        return nullptr;
    return reinterpret_cast<const xcb_selection_notify_event_t *>(ev);
}

const xcb_selection_request_event_t *selreqevent(const xcb_generic_event_t *ev)
{
    if ((ev->response_type & 0x7F) != XCB_SELECTION_REQUEST)
        return nullptr;
    return reinterpret_cast<const xcb_selection_request_event_t *>(ev);
}

void XClipGlue::handle_x_event(const xcb_generic_event_t *gev)
{
    if (auto *ev = selclearevent(gev)) {
        handle_selection_clear(ev);
    } else if (auto *ev = selnotevent(gev)) {
        handle_selection_notify(ev);
    } else if (auto *ev = selreqevent(gev)) {
        handle_selection_request(ev);
    } else if (auto *ev = propevent(gev)) {
        handle_property_notify(ev);
    }
}

void XClipGlue::handle_selection_clear(const xcb_selection_clear_event_t *ev)
{
    string selname = atom_to_string(ev->selection);
    auto it = selections_owned.find(selname);
    if (it != selections_owned.end())
        selections_owned.erase(it);
    vector<string> cmd { "acq", selname };
    send_command(cmd);
}

void XClipGlue::handle_selection_notify(const xcb_selection_notify_event_t *ev)
{
    auto it = pending_reqs_r.find(ev->requestor);
    if (it == pending_reqs_r.end())
        return;
    auto &req = *it->second;

    if (req.got_sel_notify)
        return; // just in case - don't do this work more than once
    req.got_sel_notify = true;

    if (ev->property == XCB_NONE) {
        for (size_t i = 0; i < req.ntargets; i++)
            req.results.push_back(nullptr);
    } else if (req.multiple) {
        Prop prop = get_property(it->first, ev->property);
        for (size_t i = 0; i < req.ntargets; i++) {
            bool refused = false;
            size_t offset = 8*i;
            if (prop.data.size() >= offset + 8) {
                xcb_atom_t atomcheck[2];
                memcpy(&atomcheck, prop.data.data() + offset, 8);
                // There seems to be some disagreement over how you
                // signal a failed target in MULTIPLE selection
                // transfers. xterm zeroes out the _first_ atom of the
                // pair in the main destination property, representing
                // the target; GTK3 zeroes out the second one,
                // representing the destination property for that
                // target. My reading of ICCCM suggests the former,
                // but the latter is more consistent with how a
                // non-MULTIPLE selection is refused. Handle both in
                // the same way.
                if (atomcheck[0] == XCB_NONE || atomcheck[1] == XCB_NONE)
                    refused = true;
            }

            xcb_atom_t thispropatom = atom_from_string(our_multidest_prop(i));
            req.propatoms.push_back(thispropatom);
            if (refused) {
                req.results.push_back(nullptr);
            } else {
                get_sel_property(req, it->first, thispropatom);
            }
        }
        if (!prop.type.empty())
            xcb_delete_property(conn, it->first, ev->property);
    } else {
        req.propatoms.push_back(ev->property);
        get_sel_property(req, it->first, ev->property);
    }

    if (req.incrs_in_progress.empty())
        send_rsp(it->first);
}

void XClipGlue::get_sel_property(PendingReqR &req, xcb_window_t window,
                                 xcb_atom_t property)
{
    auto match = [window, property](const xcb_generic_event_t *ev) {
        auto pev = propevent(ev);
        return (pev && pev->window == window && pev->atom == property &&
                pev->state == XCB_PROPERTY_NEW_VALUE);
    };
    discard_events(match);

    Prop prop = get_property(window, property, true);
    if (prop.type == "INCR" && prop.format == 32 && prop.data.size() == 4) {
        req.incrs_in_progress.insert(req.results.size());
        req.results.push_back(nullptr);
    } else if (prop.type.empty()) {
        req.results.push_back(nullptr);
    } else {
        req.results.push_back(make_unique<Prop>(prop));
    }
}

void XClipGlue::read_incr(xcb_window_t window, xcb_atom_t property, size_t i)
{
    auto &req = *pending_reqs_r[window];

    if (!req.incrs_in_progress.count(i))
        return;                        // spurious

    Prop prop = get_property(window, property, true);
    if (!req.results[i]) {
        req.results[i] = make_unique<Prop>(prop);
    } else {
        req.results[i]->data += prop.data;
    }
    if (prop.data.empty()) {
        req.incrs_in_progress.erase(i);
        if (req.incrs_in_progress.empty())
            send_rsp(window);
    }
}

void XClipGlue::send_rsp(xcb_window_t window)
{
    auto &req = *pending_reqs_r[window];

    vector<string> cmd { "rsp", PercentEscaper(req.id, " ") };
    for (auto &p: req.results)
        cmd.push_back(p ? encode_prop(*p) : "none");
    send_command(cmd);

    pending_reqs_r.erase(window);
    xcb_destroy_window(conn, window);
}

string XClipGlue::invent_id() {
    string toret = next_id;

    size_t i = next_id.size();
    while (i > 0) {
        if (next_id[i-1] == '9') {
            next_id[i-1] = '0';
            i--;
        } else {
            next_id[i-1]++;
            break;
        }
    }
    if (i == 0)
        next_id = "1" + next_id;

    return toret;
}

void XClipGlue::handle_selection_request(
    const xcb_selection_request_event_t *ev)
{
    string selname = atom_to_string(ev->selection);

    string id = invent_id();

    auto &req = pending_reqs_w[id];
    req.selection = ev->selection;
    req.timestamp = ev->time;
    req.target = ev->target;
    req.window = ev->requestor;
    req.master_dest = ev->property;
    if (req.master_dest == XCB_NONE) {
        // ICCCM says that it's a permitted but deprecated option to
        // not bother to name a destination window property. The
        // selection owner is supposed to make one up in that case,
        // and ICCCM recommends just reusing the target atom.
        req.master_dest = ev->target;
    }

    {
        auto it = selections_owned.find(selname);
        req.sel_timestamp = (it == selections_owned.end() ?
                             XCB_TIME_CURRENT_TIME : it->second.time_acquired);
    }

    vector<xcb_atom_t> target_atoms;
    target_atoms.push_back(ev->target);
    if (target_atoms[0] == atom_from_string("MULTIPLE")) {
        req.multiple = true;
        Prop prop = get_property(req.window, req.master_dest);
        size_t n = prop.data.size() / 8;
        target_atoms.clear();
        for (size_t i = 0; i < n; i++) {
            xcb_atom_t target, dest;
            memcpy(&target, prop.data.data() + 8*i, 4);
            memcpy(&dest, prop.data.data() + 8*i + 4, 4);
            target_atoms.push_back(target);
            req.destinations.push_back(dest);
        }
    } else {
        req.destinations.push_back(req.master_dest);
    }

    req.targets = atoms_to_strings(target_atoms);

    vector<string> cmd {
        "req", PercentEscaper(selname, " "), PercentEscaper(id, " ") };
    for (auto &target: req.targets)
        cmd.push_back(target);
    send_command(cmd);
}

xcb_timestamp_t XClipGlue::get_server_time()
{
    xcb_atom_t timestamp_atom = atom_from_string("TIMESTAMP");
    xcb_change_property(conn, XCB_PROP_MODE_APPEND, ownerwin, timestamp_atom,
                        XCB_ATOM_STRING, 8,
                        0, reinterpret_cast<const unsigned char *>(""));
    event_ptr ev = wait_for_event(
        [this, timestamp_atom](const xcb_generic_event_t *ev) {
            auto pev = propevent(ev);
            return (pev && pev->window == ownerwin &&
                    pev->atom == timestamp_atom);
        });
    return propevent(ev.get())->time;
}

auto XClipGlue::get_property(xcb_window_t window, xcb_atom_t property,
                             bool delete_it) -> Prop
{
    Prop prop;

    while (true) {
        xcb_get_property_cookie_t cookie = xcb_get_property(
            conn, delete_it, window, property, XCB_GET_PROPERTY_TYPE_ANY,
            prop.data.size(), 16384);
        xcb_get_property_reply_t *reply = xcb_get_property_reply(
            conn, cookie, nullptr);

        if (!reply || reply->type == XCB_NONE) {
            prop.type = "";
            return prop;
        }

        prop.format = reply->format;
        prop.type = atom_to_string(reply->type);
        prop.data += string((char *)xcb_get_property_value(reply),
                             xcb_get_property_value_length(reply));
        bool done = reply->bytes_after == 0;
        free(reply);

        if (done)
            return prop;
    }
}

void XClipGlue::write_property(xcb_window_t window, xcb_atom_t dest,
                               const Prop &p)
{
    // A simple wrapper on xcb_change_property, which fills in some
    // boring parameters and also deals with the fact that
    // xcb_change_property wants the data length in units of 'format',
    // not in bytes.
    xcb_change_property(conn, XCB_PROP_MODE_REPLACE, window, dest,
                        atom_from_string(p.type), p.format,
                        p.data.size() / (p.format/8), p.data.data());
}

void XClipGlue::write_sel_property(xcb_window_t window, xcb_atom_t dest,
                                   const Prop &p)
{
    if (p.data.size() <= INCR_THRESHOLD) {
        // The easy case: no incremental transfer needed.
        write_property(window, dest, p);
        return;
    }

    IncrW iw;
    iw.pos = 0;
    iw.value = p;

    discard_events([window, dest](const xcb_generic_event_t *ev) {
            auto pev = propevent(ev);
            return (pev && pev->window == window && pev->atom == dest &&
                    pev->state == XCB_PROPERTY_DELETE);
        });

    if (incrs_w.find(window) == incrs_w.end()) {
        // This is our first reason to need to find out about
        // PropertyNotify events on this window. Enable them.
        uint32_t event_mask = XCB_EVENT_MASK_PROPERTY_CHANGE;
        xcb_change_window_attributes(conn, window,
                                     XCB_CW_EVENT_MASK, &event_mask);
    }

    uint32_t len = p.data.size();
    Prop incr_p { "INCR", 32, string(reinterpret_cast<char *>(&len), 4) };
    write_property(window, dest, incr_p);

    incrs_w[window][dest] = iw;
}

void XClipGlue::write_sel_property_increment(
    xcb_window_t window, xcb_atom_t dest, IncrW &iw)
{
    Prop sub_p(iw.value, iw.pos, INCR_BLOCKSIZE);
    write_property(window, dest, sub_p);
    iw.pos += sub_p.data.size();

    if (sub_p.data.size() == 0) {
        // We've finished this incremental transfer, and sent the
        // zero-length record that signals EOF to the recipient. Erase
        // our record of it.
        incrs_w[window].erase(dest);

        if (incrs_w[window].empty()) {
            incrs_w.erase(window);

            // That was our last remaining reason to need to find
            // out about PropertyNotify events on this window.
            // Turn them off.
            uint32_t event_mask = 0;
            xcb_change_window_attributes(
                conn, window, XCB_CW_EVENT_MASK, &event_mask);
        }
    }
}

void XClipGlue::handle_property_notify(const xcb_property_notify_event_t *ev)
{
    if (ev->state == XCB_PROPERTY_DELETE) {
        auto it = incrs_w.find(ev->window);
        if (it != incrs_w.end()) {
            for (auto &kv_dest: it->second) {
                if (ev->atom == kv_dest.first) {
                    write_sel_property_increment(ev->window, ev->atom,
                                                 kv_dest.second);
                    return;
                }
            }
        }
    }

    if (ev->state == XCB_PROPERTY_NEW_VALUE) {
        auto it = pending_reqs_r.find(ev->window);
        if (it != pending_reqs_r.end() && it->second->got_sel_notify) {
            auto &req = *it->second;
            for (size_t i = 0, e = req.propatoms.size(); i < e; i++)
                if (req.propatoms[i] == ev->atom) {
                    read_incr(ev->window, ev->atom, i);
                    return;
                }
        }
    }
}

xcb_window_t XClipGlue::make_window(xcb_window_t parent)
{
    xcb_window_t w = xcb_generate_id(conn);
    static const uint32_t value_list[] = {
        XCB_EVENT_MASK_PROPERTY_CHANGE
    };
    xcb_create_window(conn,
                      XCB_COPY_FROM_PARENT, /* depth */
                      w, parent,            /* new win, parent win */
                      0, 0, 1, 1,           /* x,y,w,h */
                      0,                    /* border */
                      XCB_COPY_FROM_PARENT, /* class */
                      XCB_COPY_FROM_PARENT, /* visual */
                      XCB_CW_EVENT_MASK, value_list);
    return w;
}

bool XClipGlue::acquire_selection(const string &name)
{
    xcb_timestamp_t timestamp = get_server_time();
    xcb_atom_t sel_atom = atom_from_string(name);
    xcb_set_selection_owner(conn, ownerwin, sel_atom, timestamp);

    // Now call get_selection_owner(), and see if it's us. If it
    // isn't, then we failed to get the selection, and should return
    // failure.
    auto cookie = xcb_get_selection_owner(conn, sel_atom);
    auto *reply = xcb_get_selection_owner_reply(conn, cookie, nullptr);
    if (reply && reply->owner == ownerwin) {
        auto &sel = selections_owned[name];
        sel.atom = atom_from_string(name);
        sel.time_acquired = timestamp;
        return true;
    } else {
        return false;
    }
}

void XClipGlue::request_selection(const string &selname, const string &id,
                                  const vector<string> &targets)
{
    xcb_timestamp_t timestamp = get_server_time();

    auto req = make_unique<PendingReqR>();

    req->id = id;
    req->ntargets = targets.size();
    assert(req->ntargets > 0);
    req->multiple = req->ntargets > 1;

    vector<string> atom_names;

    size_t selection_pos = atom_names.size();
    atom_names.push_back(selname);

    size_t target_pos = atom_names.size();
    atom_names.push_back(req->multiple ? "MULTIPLE" : targets[0]);

    size_t dest_pos = atom_names.size();
    atom_names.push_back(our_dest_prop());

    size_t multiple_base = atom_names.size();
    if (req->multiple) {
        for (size_t i = 0, e = targets.size(); i < e; i++) {
            atom_names.push_back(targets[i]);
            atom_names.push_back(our_multidest_prop(i));
        }
    }

    xcb_window_t w = make_window(ownerwin);

    vector<xcb_atom_t> atoms = atoms_from_strings(atom_names);

    xcb_atom_t target = atoms[target_pos], dest = atoms[dest_pos];
    if (req->multiple)
        xcb_change_property(conn, XCB_PROP_MODE_REPLACE, w,
                            dest, atom_from_string("ATOM_PAIR"),
                            32, 2 * targets.size(),
                            atoms.data() + multiple_base);

    xcb_atom_t selection = atoms[selection_pos];
    xcb_convert_selection(conn, w, selection, target, dest, timestamp);
    pending_reqs_r[w] = move(req);
}

xcb_atom_t XClipGlue::atom_from_string(string name)
{
    auto vec = atoms_from_strings({name});
    return vec[0];
}

vector<xcb_atom_t> XClipGlue::atoms_from_strings(vector<string> names)
{
    size_t n = names.size();
    vector<xcb_atom_t> atoms(n);
    vector<xcb_intern_atom_cookie_t> cookies;
    for (size_t i = 0; i < n; i++) {
	const auto &name = names[i];
        auto it = atoms_by_name.find(name);
        if (it != atoms_by_name.end()) {
            atoms[i] = it->second;
        } else {
            atoms[i] = XCB_NONE;
            cookies.push_back(xcb_intern_atom(conn, false, name.size(),
                                              name.c_str()));
        }
    }

    if (!cookies.empty()) {
        size_t j = 0;
        for (size_t i = 0; i < n; i++) {
            if (atoms[i] == XCB_NONE) {
                assert(j < cookies.size());
                auto *reply = xcb_intern_atom_reply(conn, cookies[j], nullptr);
                if (!reply)
                    FATAL(false) << "failed to intern atom " << names[i];
                atoms_by_name[names[i]] = reply->atom;
                atoms_by_id[reply->atom] = names[i];
                atoms[i] = reply->atom;
                free(reply);
                j++;
            }
        }
        assert(j == cookies.size());
    }

    return atoms;
}

string XClipGlue::atom_to_string(xcb_atom_t atom)
{
    auto vec = atoms_to_strings({atom});
    return vec[0];
}

vector<string> XClipGlue::atoms_to_strings(vector<xcb_atom_t> atoms)
{
    size_t n = atoms.size();
    vector<string> names(n);
    vector<bool> got(n);
    vector<xcb_get_atom_name_cookie_t> cookies;
    for (size_t i = 0; i < n; i++) {
	xcb_atom_t atom = atoms[i];
        if (atom == XCB_NONE) {
            // Strictly speaking, it's wrong to return "" as the
            // translation of the None atom, because "" is a perfectly
            // legal real atom name. But we have to return
            // _something_, because we're returning a vector<string>
            // and there's no null value for a C++ string. So it's up
            // to the caller to recognise that it shouldn't actually
            // trust what it got from this conversion.
            got[i] = true;
            names[i] = "";
        } else {
            auto it = atoms_by_id.find(atom);
            if (it != atoms_by_id.end()) {
                got[i] = true;
                names[i] = it->second;
            } else {
                got[i] = false;
                cookies.push_back(xcb_get_atom_name(conn, atom));
            }
        }
    }

    if (!cookies.empty()) {
        size_t j = 0;
        for (size_t i = 0; i < n; i++) {
            if (!got[i]) {
                assert(j < cookies.size());
                auto *reply = xcb_get_atom_name_reply(
                    conn, cookies[j], nullptr);
                if (!reply)
                    FATAL(false) << "failed to get name for atom " << atoms[i];
                string name(xcb_get_atom_name_name(reply),
                            xcb_get_atom_name_name_length(reply));
                atoms_by_name[name] = atoms[i];
                atoms_by_id[atoms[i]] = name;
                names[i] = name;
                free(reply);
                j++;
            }
        }
        assert(j == cookies.size());
    }

    return names;
}

int main(int argc, char **argv)
{
    const char *display = nullptr;
    string display_str;
    const char *display2 = nullptr;
    string display2_str;
    bool stdio = false, remote = false, dbldisp = false;
    bool doing_opts = true;
    bool verbose = false;
    bool child = false;
    bool active = false, default_active = true;
    int argpos = 1;
    vector<string> selections;
    vector<string> subcommand;

    static const char help[] =
        "usage: xclipglue [ options ] ( <other-display> | -stdio | "
        "-remote cmd [args] )\n"
        "modes:   <other-display>        unify with this X display\n"
        "         -stdio                 act as a server on this display\n"
        "         -remote <cmd> <args>   run the other end remotely "
        "(e.g. via ssh)\n"
        "options: -display <display>     override primary X display\n"
        "         -s <selection>         specify a selection to handle\n"
        "                                    (can use this more than once)\n"
        "         -active                propagate primary display to remote\n"
        "         -passive               expect remote display to propagate to"
        " us\n"
        "         -v                     log activity in the wire protocol\n"
        "also:    xclipglue --version    report version number\n"
        "         xclipglue --help       display this help text\n"
        "         xclipglue --licence    display the (MIT) licence text\n";

    static const char licence[] =
        "xclipglue is copyright 2020 Simon Tatham.\n"
        "\n"
        "Permission is hereby granted, free of charge, to any person\n"
        "obtaining a copy of this software and associated documentation files\n"
        "(the \"Software\"), to deal in the Software without restriction,\n"
        "including without limitation the rights to use, copy, modify, merge,\n"
        "publish, distribute, sublicense, and/or sell copies of the Software,\n"
        "and to permit persons to whom the Software is furnished to do so,\n"
        "subject to the following conditions:\n"
        "\n"
        "The above copyright notice and this permission notice shall be\n"
        "included in all copies or substantial portions of the Software.\n"
        "\n"
        "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\n"
        "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\n"
        "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\n"
        "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS\n"
        "BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN\n"
        "ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN\n"
        "CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n"
        "SOFTWARE.\n"
        ;

    static const char version[] =
        "xclipglue, unknown version"; /*---buildsys-replace---*/

    auto get_arg = [&](string &s) {
        if (argpos >= argc)
            return false;
        s = argv[argpos++];
        return true;
    };

    string arg;
    auto expect_param = [&](string arg) {
        string param;
        if (!get_arg(param))
            FATAL(false) << "option '" << arg << "' expects a parameter";
        return param;
    };

    while (get_arg(arg)) {
        if (doing_opts && arg.size() > 1 && arg[0] == '-') {
            if (arg == "--") {
                doing_opts = false;
            } else if (arg == "-display" || arg == "-disp" ||
                       arg == "--display" || arg == "--disp") {
                display_str = expect_param(arg);
                display = display_str.c_str();
            } else if (arg == "-remote" || arg == "--remote") {
                subcommand.push_back(expect_param(arg));
                while (get_arg(arg))
                    subcommand.push_back(arg);
                remote = true;
            } else if (arg == "-stdio" || arg == "--stdio") {
                stdio = true;
            } else if (arg == "-v") {
                verbose = true;
            } else if (arg == "-s") {
                selections.push_back(expect_param(arg));
            } else if (arg == "-active" || arg == "--active") {
                default_active = false;
                active = true;
            } else if (arg == "-passive" || arg == "--passive") {
                default_active = false;
                active = false;
            } else if (arg == "-version" || arg == "--version") {
                cout << version << endl;
                return 0;
            } else if (arg == "-help" || arg == "--help") {
                cout << help;
                return 0;
            } else if (arg == "-licence" || arg == "--licence" ||
                       arg == "-license" || arg == "--license") {
                cout << licence;
                return 0;
            } else {
                FATAL(false) << "unrecognised option '" << arg << "'";
            }
	} else {
            if (!display2) {
                display2_str = arg;
                display2 = display2_str.c_str();
                dbldisp = true;
            } else {
                FATAL(false) << "unexpected extra argument '" << arg << "'";
            }
	}
    }

    if (stdio + remote + dbldisp != 1)
        FATAL(false) << "specify exactly one of -stdio, -remote or a "
            "second display";

    XClipGlue xcs;

    if (verbose)
        xcs.set_log_stream(clog);

    if (stdio)
        xcs.setup_stdio();
    else if (remote)
        xcs.setup_subcommand(subcommand);
    else
        display = xcs.setup_double_display(display, display2, &child);

    xcs.setup_x11(display);

    if (dbldisp) {
        if (default_active)
            active = true;
        if (child)
            active = !active;
    } else if (default_active) {
        active = remote;
    }

    if (selections.empty()) {
        selections.push_back("PRIMARY");
        selections.push_back("CLIPBOARD");
    }

    if (active)
        for (auto &s: selections)
            xcs.send_acq(s);

    xcs.mainloop();
}
