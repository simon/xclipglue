# for `make release' and `make html'
DESTDIR = .

# for `make install'
PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
SCRIPTDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/man/man1
INSTALL = install
IPROG =#   flags for installing programs (default none)
IDATA = -m 0644  # flags for installing data

XCBLIB = -lxcb

all: xclipglue.1 xclipglue
man: xclipglue.1
progs: xclipglue

xclipglue: xclipglue.cpp
	$(CXX) -std=c++14 $(CPPFLAGS) $(CXXFLAGS) -o $@ $< $(XCBLIB)

%.1: %.but
	halibut --man=$@ $<

clean:
	rm -f *.1 xclipglue *.html *.tar.gz

html:
	halibut --html=xclipglue.html xclipglue.but
	mv xclipglue.html $(DESTDIR)/xclipglue.html

release: xclipglue.1
	mkdir -p reltmp/xclipglue
	ln -s ../../xclipglue.cpp reltmp/xclipglue
	ln -s ../../xclipglue.1 reltmp/xclipglue
	ln -s ../../xclipglue.but reltmp/xclipglue
	ln -s ../../Makefile reltmp/xclipglue
	tar -C reltmp -chzf $(DESTDIR)/xclipglue.tar.gz xclipglue
	rm -rf reltmp

install: install-progs install-man

install-progs: xclipglue
	mkdir -p $(BINDIR)
	$(INSTALL) $(IPROG) xclipglue $(BINDIR)/xclipglue
install-man: xclipglue.1
	mkdir -p $(MANDIR)
	$(INSTALL) $(IDATA) xclipglue.1 $(MANDIR)/xclipglue.1
